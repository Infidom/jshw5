function createNewUser() {
    let firstName;
    let lastName;

    do {
        firstName = prompt('Enter first name', [firstName]);
        lastName = prompt('Enter last name', [lastName]);
    } while (!isNaN(firstName) || !isNaN(lastName));

    let newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin: function() {
            return this.firstName[0].toLocaleLowerCase() + this.lastName.toLocaleLowerCase();
        }
    }

    Object.defineProperties(newUser, {
        firstName: {
            writable: false,
            get setFirstName() {
                return this.firstName;
            },
            set setFirstName(newFirstName) {
                this.firstName = newFirstName;
            }
        },
        lastName: {
            writable: false,
            get setLastName() {
                return this.lastName;
            },
            set setLastName(newLastName) {
                this.lastName = newLastName;
            }
        }
    })

    return newUser.getLogin;
}

let user = createNewUser();
console.log(user);